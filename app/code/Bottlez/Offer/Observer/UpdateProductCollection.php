<?php
/**
 * Description
 * 
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */
namespace Bottlez\Offer\Observer;

use Magento\Framework\Event\ObserverInterface;

class UpdateProductCollection implements ObserverInterface {
    /**
     * Add filter to product collection
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $collection = $observer->getEvent()->getCollection();
        /* @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
       /* $collection->addAttributeToFilter('type_id', array('eq' => 'simple'));
        echo '<pre>';
        print_r($collection->getSelect()->__toString());
        echo '</pre>';
        //die;*/

        return $this;
    }
}