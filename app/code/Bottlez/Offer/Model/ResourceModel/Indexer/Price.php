<?php

/**
 * Default price offer model
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */

namespace Bottlez\Offer\Model\ResourceModel\Indexer;

class Price	extends	\Magento\Catalog\Model\ResourceModel\Product\Indexer\Price\DefaultPrice {

}