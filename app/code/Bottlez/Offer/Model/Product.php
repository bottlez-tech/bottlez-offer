<?php
/**
 * Extend product model
 *
 * @author Sintsov Roman <romiras_spb@mail.ru>
 * @copyright Copyright (c) 2016, bottlez-rebuild
 */
namespace Bottlez\Offer\Model;

class Product extends \Magento\Catalog\Model\Product {

    //private $offersCollection;

    private function getProductOfferCollection() {
        /*if ($this->offersCollection) {
            return $this->offersCollection;
        }*/
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $collection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');

        $collection->addAttributeToSelect(array('price', 'bottle_qty'))
            ->addFieldToFilter('base_product_id', $this->getId())
            ->addFieldToFilter('type_id', array(
                'eq' => \Bottlez\Offer\Model\Product\Type\Offer::TYPE_CODE
            ));

        $this->offersCollection = $collection;
        return $collection;
    }

    public function getContainer() {
        if ($containerId = $this->getData('container')) {
            $container = $this->getAttributeText('container');
            if ($container >= 1000) {
                return ($container / 1000) . 'l';
            } else {
                return $container . 'ml';
            }
        }

        return false;
    }

    /**
     * Calculate from offers set minimal price per bottle
     * @return mixed
     */
    public function getMinimalPricePerBottle() {
        $collection = $this->getProductOfferCollection();

        $collection->addExpressionAttributeToSelect('price_per_bottle', '({{price}}/{{bottle_qty}})', array('price', 'bottle_qty'))
            ->setOrder('price_per_bottle', 'asc')
            ->setPageSize(1)
            ->setCurPage(1);

        return $collection->getFirstItem()->getData('price_per_bottle');
    }

    /**
     * Get product offer count
     * @return string
     */
    public function getOffersCount() {
        $collection = $this->getProductOfferCollection();
        return $collection->count();
    }
}