<?php

/**
 * Common helper
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */

namespace Bottlez\Offer\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    /** @var \Magento\Framework\ObjectManagerInterface $objectManager */
    protected $objectManager;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;

        parent::__construct($context);
    }

    /**
     * Generation product, offer SKU
     * @param \Magento\Catalog\Model\Product $product product
     */
    public function generateSku($product) {
        $this->objectManager
            ->create('Bottlez\Offer\Model\Product\Attribute\Backend\Sku')
            ->generateUniqueSku($product);
    }

    public function getFormatPrice($price, $storeId, $withCurrency = true) {
        /* @var \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrencyModel */
        $priceCurrencyModel = $this->objectManager->get('Magento\Framework\Pricing\PriceCurrencyInterface');
        if ($withCurrency) {
            $currencyCode = $this->objectManager->get('Magento\Store\Model\Store')->load($storeId)->getBaseCurrencyCode();
            return $priceCurrencyModel->format($price, false, 2, null, $currencyCode);
        } else {
            return $priceCurrencyModel->format($price, false, 2, null, null);
        }
    }

}