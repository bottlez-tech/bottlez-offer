<?php

/**
 * Offers product data view
 *
 * @author Sintsov Roman <romiras_spb@mail.ru>
 * @copyright Copyright (c) 2016, bottlez-rebuild
 */

namespace Bottlez\Offer\Block\Product\View;

use Magento\Catalog\Block\Product\AbstractProduct;

class Offers extends AbstractProduct {

    /**
     * @var Product
     */
    protected $_product = null;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    public $_objectManager;

    protected $_productFactory = null;

    protected $_currency;

    protected $_storeManager;

    /**
     * @var \Magento\Framework\Url\Helper\Data
     */
    protected $_urlHelper;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\ObjectManagerInterface $objectManager,
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Directory\Model\Currency $currency,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->_objectManager = $objectManager;
        $this->_productFactory  = $productFactory;
        $this->_currency = $currency;
        $this->_storeManager = $storeManager;
        $this->_urlHelper = $urlHelper;
        $this->productRepository = $productRepository;

        parent::__construct($context, $data);
    }

    /**
     * Get current product
     *
     * @return Product|mixed
     */
    public function getProduct() {
        if (!$this->_product) {
            $this->_product = $this->_coreRegistry->registry('product');
        }
        return $this->_product;
    }

    public function setProductId($id) {
        $this->_product = $this->productRepository->getById($id);
    }

    /**
     * @return Product
     */
    public function getOffers() {
        $product = $this->getProduct();
        $collection = $this->_productFactory->create()
                        ->getCollection()
                        ->addAttributeToSelect('*')
                        ->addFieldToFilter('base_product_id', $product->getId())
                        ->addFieldToFilter('type_id', array(
                            'eq' => \Bottlez\Offer\Model\Product\Type\Offer::TYPE_CODE
                        ))
                        ->joinField('qty',
                            'cataloginventory_stock_item',
                            'qty',
                            'product_id=entity_id',
                            '{{table}}.stock_id=1',
                            'left'
                        )
                        ->joinField('vendor_id',
                            'ced_csmarketplace_vendor_products',
                            'vendor_id',
                            'product_id=entity_id',
                             null,
                            'left'
                        );
        
        return $collection;
    }

    /**
     * Get currency symbol for current locale and currency code
     *
     * @return string
     */
    public function getCurrencySymbol() {
        return $this->_currency->getCurrencySymbol();
    }

    /**
     * Get post parameters
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getAddToCartPostParams(\Magento\Catalog\Model\Product $product) {
        $url = $this->getAddToCartUrl($product);
        return [
            'action' => $url,
            'data' => [
                'product' => $product->getEntityId(),
                \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED =>
                    $this->_urlHelper->getEncodedUrl($url),
            ]
        ];
    }

}
