<?php
/**
 * Description
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */
namespace Bottlez\Offer\Model\Plugin;

use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;

class CatalogProductFilter {

    /**
     * aroundAddFieldToFilter method
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @param \Closure                                                $proceed
     * @param                                                         $fields
     * @param null                                                    $condition
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    /*public function aroundAddFieldToFilter(ProductCollection $collection, \Closure $proceed, $fields, $condition = null) {
        // Here we can modify the collection
        $collection->getSelect()->where("e.type_id='simple'");
        //$proceed('type_id', ['eq' => 'simple']);
        return $fields ? $proceed($fields, $condition) : $collection;
    }*/

}