<?php 
/**
 * Description
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */
namespace Bottlez\Offer\Controller\Product;

class OffersToBuy extends \Magento\Framework\App\Action\Action {

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    protected $layoutFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\View\LayoutFactory $layoutFactory
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->logger = $logger;
        $this->layoutFactory = $layoutFactory;
        parent::__construct($context);
    }

    public function execute() {
        $id = (int) $this->getRequest()->getPost('id');
        $data = array(
            'success' => true
        );
        try {
            $data['html'] = $this->layoutFactory->create()
              ->createBlock('Bottlez\Offer\Block\Product\Grid\Offers')
              ->setProductId($id)
              ->setTemplate('Bottlez_Offer::product/grid/popup/offer-to-buy.phtml')
              ->toHtml();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            $data['error'] = 'Sorry, something went wrong. Please try again later.';
        }
        return $this->resultJsonFactory->create()->setData($data);
    }

}
