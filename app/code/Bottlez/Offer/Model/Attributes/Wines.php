<?php
/**
 * System attributes for wines
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */

namespace Bottlez\Offer\Model\Attributes;

class Wines extends \Magento\Framework\Model\AbstractModel {

    /** @var array mapping data by simple product */
    private $dataMapping = [
        'name'       => 'name',
        'vintage'    => 'vintage',
        'country'    => 'country_of_manufacture',
        'color'      => 'color',
        'container'  => 'container'
    ];

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('Bottlez\Offer\Model\ResourceModel\Attributes\Wines');
    }

    public function getDataMapping() {
        return $this->dataMapping;
    }

}