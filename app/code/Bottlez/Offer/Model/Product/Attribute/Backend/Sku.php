<?php

/**
 * Generate unique SKU
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */
namespace Bottlez\Offer\Model\Product\Attribute\Backend;

use Symfony\Component\Config\Definition\Exception\Exception;

class Sku extends \Magento\Catalog\Model\Product\Attribute\Backend\Sku {

    /** @var \Magento\Framework\ObjectManagerInterface $objectManager */
    protected $objectManager;

    /** @var \Magento\Framework\Escaper $escaper */
    protected $escaper;

    /** @var  \Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository */
    protected $productAttributeRepository;

    public function __construct(
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Escaper $escaper,
        \Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository
    ) {
        $this->objectManager = $objectManager;
        $this->escaper = $escaper;
        $this->productAttributeRepository = $productAttributeRepository;
        parent::__construct($string);
    }

    /**
     * Generate unique SKU by product name
     *
     * @param \Magento\Catalog\Model\Product $object product
     */
    public function generateUniqueSku($object) {
        $attributes = $object->getAttributes();
        if (!isset($attributes['sku'])
            || !($attributes['sku'] instanceof \Magento\Eav\Model\Entity\Attribute\AbstractAttribute)
        ) {
            throw new Exception(__('Not exist attribute sku'));
        }
        /** @var $galleryAttribute \Magento\Eav\Model\Entity\Attribute\AbstractAttribute */
        $attribute = $attributes['sku'];
        //var_dump($attribute->getBackend()->getTable());die;
        $this->setAttribute($attribute);

        if (!$object->getName()) {
            throw new Exception(__('Product don\'t have are name'));
        }

        $object->setSku($object->getName());
        $this->_generateUniqueSku($object);

        $sku = $this->slugify($this->escaper->escapeHtml(($this->string->splitInjection($object->getSku()))));
        $object->setSku($sku);
    }

    /**
     * Slug by Simfony way
     *
     * @param string $text slug
     * @return mixed|string
     */
    public function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}