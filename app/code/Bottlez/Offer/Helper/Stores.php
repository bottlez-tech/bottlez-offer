<?php

/**
 * Common helper
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */

namespace Bottlez\Offer\Helper;

class Stores extends \Magento\Framework\App\Helper\AbstractHelper {

    /** @var \Magento\Framework\ObjectManagerInterface $objectManager */
    protected $objectManager;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;


    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->objectManager = $objectManager;
        $this->_storeManager = $storeManager;

        parent::__construct($context);
    }


    /**
     * Get all websites
     */
    public function getWebsites()
    {
        return $this->_storeManager->getWebsites();
    }

    /**
     *  Get current website
     */

    public function getWebsite()
    {
        return $this->_storeManager->getWebsite();
    }

}