<?php
/**
 * System attributes for wines
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */
namespace Bottlez\Offer\Model\ResourceModel\Attributes;

class Wines extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('bottlez_attributes_wines', 'id');
    }
}