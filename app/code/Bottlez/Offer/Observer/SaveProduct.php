<?php

/**
 * Description
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */
namespace Bottlez\Offer\Observer;

use Magento\Framework\Event\ObserverInterface;

class SaveProduct implements ObserverInterface {

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager) {
        $this->_objectManager = $objectManager;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $product = $observer->getProduct();

        if (
            $product->getTypeId() == \Bottlez\Offer\Model\Product\Type\Offer::TYPE_CODE ||
            $product->getBaseId()
        ) {
            return;
        }

        $model = $this->_objectManager->get('Bottlez\Offer\Model\Attributes\Wines');
        $map = $model->getDataMapping();
        $data = [];
        foreach ($map as $key => $value) {
            $data[$key] = $product->getData($value);
        }

        if (empty($data)) {
            //TODO: throw Exception
        }

        $model->addData($data);
        $model->save();
        
        $product->setData('base_id', $model->getId());
        $product->save();
    }
}