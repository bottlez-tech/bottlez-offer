<?php

/**
 * Description
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */
namespace Bottlez\Offer\Observer;

use Magento\Framework\Event\ObserverInterface;

class DeleteProduct implements ObserverInterface {

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager) {
        $this->_objectManager = $objectManager;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
       /* $product = $observer->getProduct();
        $model = $this->_objectManager->get('Bottlez\Offer\Model\Attributes\Wines');
        $attributes = $model->load($product->getId(), 'product_id');
        if ($attributes && $attributes->getId()) {
            $model->delete();
        }*/
    }
}