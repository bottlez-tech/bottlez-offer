<?php

/**
 * Collection wines system attributes
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */
namespace Bottlez\Offer\Model\ResourceModel\Attributes\Wines;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('Bottlez\Offer\Model\Attributes\Wines', 'Bottlez\Offer\Model\ResourceModel\Attributes\Wines');
    }

    /*public function addSimpleProduct() {
        $this->getSelect()
            ->join(
                ['e' => 'catalog_product_entity'],
                'main_table.id = e.entity_id',
                ['*']
            );
        return $this;
    }*/
}