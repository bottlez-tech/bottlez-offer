<?php

/**
 * Offers product data view
 *
 * @author Sintsov Roman <romiras_spb@mail.ru>
 * @copyright Copyright (c) 2016, bottlez-rebuild
 */

namespace Bottlez\Offer\Block\Product\Grid;

class Offers extends \Magento\Framework\View\Element\Template {

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        $this->productRepository = $productRepository;

        parent::__construct($context, $data);
    }

    public function setProductId($id) {
        $this->product = $this->productRepository->getById($id);
    }
}
