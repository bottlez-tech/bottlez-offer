<?php

/**
 * Offer product type model
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */

namespace Bottlez\Offer\Model\Product\Type;

class Offer extends \Magento\Catalog\Model\Product\Type\AbstractType {

    /**
     * Product type code
     */
    const TYPE_CODE = 'offer';

    /**
     * Delete data specific for Simple product type
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return void
     */
    public function deleteTypeSpecificData(\Magento\Catalog\Model\Product $product) {
    }

}